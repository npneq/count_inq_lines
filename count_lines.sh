git clone https://gitlab.com/npneq/inq.git
rm -f lines.dat.tmp
cd inq
git pull
for x in `git log master  | grep ^commit | grep -v a18bfe132600dcf7174e4424dadfc1bcefde1fbc | awk '{print $2}'`
   do
   git clean -f >& /dev/null
   git checkout $x 
   date=` git show -s --format=%at $x`
   total=`cat src/*/*pp | wc -l`
   nohead=`grep -s -A1000000000 -E 'Foundation|mozilla|' src/*/*pp | grep -v ^-- |  wc -l`
   oldtests=`grep -s -A1000000000 -E 'Foundation|mozilla|' src/tests/*pp | grep -v ^-- |  wc -l`   
   utests=`grep -s -A1000000000 TEST_CASE src/*/*pp | grep -v ^-- |  wc -l`
   itests=`grep -s -A1000000000 -E 'Foundation|mozilla|' tests/*pp speed_tests/*pp src/tests/*pp | grep -v ^-- |  wc -l`
   echo $date $total $(($nohead - $oldtests - $utests)) $utests $itests $x >> ../lines.dat.tmp
done
cd ..
mv lines.dat.tmp lines.dat
