This is a simple set of scripts that counts the code lines in [inq](https://gitlab.com/npneq/inq).
It is updated daily, so the image below shows the updated count.

![Evolution of the number of code lines in inq](https://npneq.gitlab.io/count_inq_lines/lines.png)

You can also download the file in different formats: [pdf](https://npneq.gitlab.io/count_inq_lines/lines.pdf) [png](https://npneq.gitlab.io/count_inq_lines/lines.png) [eps](https://npneq.gitlab.io/count_inq_lines/lines.eps)
