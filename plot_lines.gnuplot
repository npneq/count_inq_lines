fgcolor="black"
set xdata time
set timefmt "%s"
set format x "%y/%m" # or whatever
set ytics format '%gk'

set ylabel "Lines of code" textcolor rgb fgcolor
set xlabel "Date [Year/Month]" textcolor rgb fgcolor

set key inside bottom right textcolor rgb fgcolor

set border linecolor rgb fgcolor 

today=`date +%s`
set term "pdfcairo"
set out "lines.pdf"
plot [1558000000:today] "<sort -n lines.dat | grep -v 51fa7bb63e31cb6dc59d31904f4a7a9ad3f83783 | grep -v c91676adeb84f723cefcceb50abf4b3a111fc5c9 | grep -v 54df2c263ab709a6d6028e7ec59e57f1e699cd0b" u 1:($3/1000) w l t "Code (without tests)" lw 2, "" u 1:(($4 + $5)/1000) w l t "Tests" lw 2

set term "pngcairo"
set out "lines.png"

replot

set term "epscairo"
set out "lines.eps"

replot

